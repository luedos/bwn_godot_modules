
class PP_array_view:
	def __init__(self, val):
		self.val = val
		self.array_ptr = val['m_ptr']
		self.is_empty = val['m_size'] == 0
		self.size = val['m_size']

	def to_string(self):
		return 'size: {}'.format(self.size)

	def display_hint(self):
		return 'array'

	def children(self):
		count = 0
		while count < self.size:
			yield str(count), (self.array_ptr + count).dereference()
			count += 1

class PP_simple_list:
	def __init__(self, val):
		self.val = val
		self.first_ptr = val['m_first']
		self.size = self.compute_count(self.first_ptr)

	def compute_count(self, first_node):
		count = 0
		current_node = first_node
		while current_node != 0:
			count += 1
			# This is kind of a costyl, because even though this make sense in terms of c++ code,
			# to call specific function next() instead of simple variable, we can't really call this function in gdb.
			current_node = current_node['m_next']

		return count

	def to_string(self):
		return 'size: {}'.format(self.size)

	def display_hint(self):
		return 'array'

	def children(self):
		count = 0
		current_node = self.first_ptr
		while current_node != 0:
			yield f'[{count}]', current_node.dereference()
			# This is kind of a costyl, because even though this make sense in terms of c++ code,
			# to call specific function next() instead of simple variable, we can't really call this function in gdb.
			current_node = current_node['m_next']
			count += 1


class PP_StringId:
	def __init__(self, val):
		self.val = val
		self.hash = val['m_hash']
		self.is_string_name = str(self.hash.type) == "StringName"
		if self.is_string_name:
			self.is_valid = self.hash['_data'] != 0
		else:
			self.is_valid = self.hash != 0

	def to_string(self):
		if self.is_string_name:
			return self.hash.format_string()
		elif self.is_valid:
			return f'0x{self.hash:08x}'
		else:
			'empty id'

	def display_hint(self):
		return None

class PP_WeakRef:
	def __init__(self, val):
		self.val = val
		self.value_type = self.val.type.template_argument(0)
		self.rc_ptr = val['m_objectRefCount']
		self.object_ptr = 0
		self.ref_count = 0
		if self.rc_ptr != 0:
			self.object_ptr = self.get_atomic_ptr(self.rc_ptr['_ptr'])
			self.ref_count = self.rc_ptr['_users'].format_string()

	def get_atomic_ptr(self, ptr_val):
		return ptr_val['_M_b']['_M_p']

	def to_string(self):
		return 'ref count: {}'.format(self.ref_count)

	def display_hint(self):
		return None

	def children(self):
		if self.rc_ptr == 0:
			yield '<Reference not set>', 0
		elif self.object_ptr == 0:
			yield '<Object not valid>', 0
		else:
			yield str(self.value_type), self.object_ptr.cast(self.value_type.pointer()).dereference()

class PP_UniqueRef:
	def __init__(self, val):
		self.val = val
		self.value_type = self.val.type.template_argument(0)
		self.rc_ptr = val['m_objectRef']['m_objectRefCount']
		self.object_ptr = 0
		self.ref_count = 0
		if self.rc_ptr != 0:
			self.object_ptr = self.get_atomic_ptr(self.rc_ptr['_ptr'])
			self.ref_count = self.rc_ptr['_users'].format_string()

	def get_atomic_ptr(self, ptr_val):
		return ptr_val['_M_b']['_M_p']

	def to_string(self):
		return 'ref count: {}'.format(self.ref_count)

	def display_hint(self):
		return None

	def children(self):
		if self.rc_ptr == 0:
			yield '<Reference not set>', 0
		elif self.object_ptr == 0:
			yield '<Object not valid>', 0
		else:
			yield str(self.value_type), self.object_ptr.cast(self.value_type.pointer()).dereference()

def my_pp_func(val):
	clear_type = val.type.strip_typedefs().unqualified()
	if str(clear_type.name).find('bwn::basic_array_view') == 0: return PP_array_view(val)
	if str(clear_type.name).find('bwn::simple_list') == 0: return PP_simple_list(val)
	if str(clear_type.name) == 'bwn::StringId': return PP_StringId(val)
	if str(clear_type.name).find('bwn::WeakRef') == 0: return PP_WeakRef(val)
	if str(clear_type.name).find('bwn::UniqueRef') == 0: return PP_UniqueRef(val)

	return None

gdb.pretty_printers.append(my_pp_func)