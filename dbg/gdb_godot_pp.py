import re
def retrive_string_from_String(string_val):
	string_ptr = string_val['_cowdata']['_ptr']
	if string_ptr == 0:
		return ''
	else:
		return string_ptr.string()

class PP_String:
	def __init__(self, val):
		self.val = val
		self.string_ptr = val['_cowdata']['_ptr']
		self.is_empty = self.string_ptr == 0
		if self.is_empty:
			self.size = 0
			self.string = ''
		else:
			self.size = int((self.string_ptr.reinterpret_cast(gdb.lookup_type('uint32_t').pointer()) - 1).dereference()) - 1
			self.string = self.string_ptr.string()

	def to_string(self):
		if self.is_empty:
			return 'empty'
		else:
			return self.string

	def display_hint(self):
		if self.is_empty:
			return None
		else:
			return 'string'

	# For now, mostly because of the bug, we will not show any children in a string
	# https://youtrack.jetbrains.com/issue/CPP-15217
	# def children(self):
	# 	if self.is_empty:
	# 		yield 'string', None
	# 	else:
	# 		yield 'string', self.string_ptr
	#
	# 	yield 'size', self.size

class PP_StringName:
	def __init__(self, val):
		self.val = val
		self.data_ptr = val['_data']
		self.is_empty = self.data_ptr == 0
		if self.is_empty:
			self.hash = 0
			self.string = ''
		else:
			data = self.data_ptr.dereference()
			self.hash = int(data['hash'])
			cname_ptr = data['cname']
			if cname_ptr == 0:
				self.string = retrive_string_from_String(data['name'])
			else:
				self.string = cname_ptr.string()

	def to_string(self):
		if self.is_empty:
			return 'empty id'
		else:
			return f'"{self.string}" (0x{self.hash:08x})'

	def display_hint(self):
		return None

	# For now, mostly because of the bug, we will not show any children in a string
	# https://youtrack.jetbrains.com/issue/CPP-15217
	# def children(self):
	# 	pass

class PP_Map:
	def __init__(self, val):
		self.val = val

		root_ptr = val['_data']['_root']
		nil_ptr = val['_data']['_nil']

		if root_ptr == 0 or root_ptr['left'] == nil_ptr:
			self.front_ptr = 0
		else:
			self.front_ptr = root_ptr['left']
			left_ptr = self.front_ptr['left']
			while left_ptr != nil_ptr:
				self.front_ptr = left_ptr
				left_ptr = left_ptr['left']

		self.size = int(val['_data']['size_cache'])

	# for now it wouldn't make a difference because of https://youtrack.jetbrains.com/issue/CPP-15217
	# but we still will have simple string
	def to_string(self):
		return f'{{ size: {self.size} }}'

	def display_hint(self):
		return 'array'

	def children(self):
		current_ptr = self.front_ptr
		while current_ptr != 0:
			yield current_ptr['_key'].format_string(raw=False), current_ptr['_value']
			current_ptr = current_ptr['_next']

class PP_HashMap:
	def __init__(self, val):
		self.val = val
		self.head_element = val['head_element']
		self.size = int(val['num_elements'])

	# for now it wouldn't make a difference because of https://youtrack.jetbrains.com/issue/CPP-15217
	# but we still will have simple string
	def to_string(self):
		return f'{{ size: {self.size} }}'

	def display_hint(self):
		return 'array'

	def children(self):
		current_element = self.head_element
		while current_element != 0:
			yield current_element['data']['key'].format_string(raw=False), current_element['data']['value']
			current_element = current_element['next']

class PP_HashMapElement:
	def __init__(self, val):
		self.val = val
		self.data = val['data']

	# for now it wouldn't make a difference because of https://youtrack.jetbrains.com/issue/CPP-15217
	# but we still will have simple string
	def to_string(self):
		return f'{{ { self.val.type.strip_typedefs() } }}'

	def display_hint(self):
		return None

	def children(self):
		yield 'key', self.data['key']
		yield 'value', self.data['value']

class PP_HashMapIterator:
	def __init__(self, val):
		self.val = val
		self.element = val['E']

	# for now it wouldn't make a difference because of https://youtrack.jetbrains.com/issue/CPP-15217
	# but we still will have simple string
	def to_string(self):
		return f'{{ { self.val.type.strip_typedefs() } }}'

	def display_hint(self):
		return None

	def children(self):
		if self.element != 0:
			yield 'key', self.element['data']['key']
			yield 'value', self.element['data']['value']
		else:
			yield '<empty>', ''

class PP_List:
	def __init__(self, val):
		self.val = val

		data_ptr = val['_data']
		if data_ptr == 0:
			self.front_ptr = 0
			self.size = 0
			return

		self.front_ptr = data_ptr['first']
		self.size = int(data_ptr['size_cache'])

	# for now it wouldn't make a difference because of https://youtrack.jetbrains.com/issue/CPP-15217
	# but we still will have simple string
	def to_string(self):
		return f'{{ size: {self.size} }}'

	def display_hint(self):
		return 'array'

	def children(self):
		current_ptr = self.front_ptr
		index = 0
		while current_ptr != 0:
			yield f'[{index}]', current_ptr['value']
			index += 1
			current_ptr = current_ptr['next_ptr']

class PP_Vector:
	def __init__(self, val):
		self.val = val
		self.array_ptr = val['_cowdata']['_ptr']
		self.is_empty = self.array_ptr == 0
		if self.is_empty:
			self.size = 0
		else:
			self.size = int((self.array_ptr.reinterpret_cast(gdb.lookup_type('uint32_t').pointer()) - 1).dereference()) - 1

	# for now it wouldn't make a difference because of https://youtrack.jetbrains.com/issue/CPP-15217
	# but we still will have simple string
	def to_string(self):
		return f'{{ size: {self.size} }}'

	def display_hint(self):
		return 'array'

	def children(self):
		count = 0
		while count < self.size:
			yield str(count), (self.array_ptr + count).dereference()
			count += 1

def my_pp_func(val):
	clear_type = val.type.strip_typedefs()
	if str(clear_type.name) == 'String': return PP_String(val)
	elif str(clear_type.name) == 'StringName': return PP_StringName(val)
	elif str(clear_type.name).find('Map<') == 0: return PP_Map(val)
	elif str(clear_type.name).find('List<') == 0: return PP_List(val)
	elif str(clear_type.name).find('Vector<') == 0: return PP_Vector(val)
	elif re.match('HashMap<.+::(?:Const)?Iterator', str(clear_type.name)): return PP_HashMapIterator(val)
	elif str(clear_type.name).find('HashMapElement<') == 0: return PP_HashMapElement(val)
	elif str(clear_type.name).find('HashMap<') == 0: return PP_HashMap(val)

	return None

gdb.pretty_printers.append(my_pp_func)